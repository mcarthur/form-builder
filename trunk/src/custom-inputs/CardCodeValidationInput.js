import type { Parameters } from '@ginkgo-bioworks/react-json-schema-form-builder';
import React, { useState } from 'react';
import Editor from 'react-simple-code-editor';
import { highlight, languages } from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';
import "prismjs/themes/prism.css";

const validationPredicateTemplate = `function validate(value, moment, _) {
  /*
    This function should return:
      • true if validation is successful,
      • false otherwise.

    Parameters:
      • value: The field value to validate.
        It can be an object or a primitive.

      • moment: momentjs instance
        (https://momentjs.com), to help
        with date/time validations. 

      • _: lodash instance (https://lodash.com),
        to help with general validations. 
  */
  return true;
}`;

export function CardCodeValidationInput({
  parameters,
  onChange,
}: {
  parameters: Parameters,
  onChange: (newParams: Parameters) => void,
}) {

  const [validationCode, setValidationCode] = useState(validationPredicateTemplate)

  const handleValidationCodeChange = (code) => {
    setValidationCode(code);
    onChange({
      ...parameters,
      validationCode: code,
    });
  }

  return (
    <div>
      <h4>
        Javascript Validation Predicate
      </h4>
      <Editor
        key='validationCode'
        value={validationCode}
        onValueChange={handleValidationCodeChange}
        highlight={code => highlight(code, languages.js)}
        padding={10}
        style={{
          fontFamily: '"Fira code", "Fira Mono", monospace',
          fontSize: 12,
        }}
      />
    </div>
  )
}
