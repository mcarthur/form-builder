import type { Parameters } from '@ginkgo-bioworks/react-json-schema-form-builder';
import React from 'react';
import { Input } from 'reactstrap';

export function DateField({
  parameters,
  onChange,
}: {
  parameters: Parameters,
  onChange: (newParams: Parameters) => void,
}) {
  return (
    <React.Fragment>
      <h5>Default date</h5>
      <Input
        value={parameters.default || ''}
        placeholder='Default'
        type='date'
        onChange={(ev: SyntheticInputEvent<HTMLInputElement>) =>
          onChange({ ...parameters, default: ev.target.value })
        }
        className='card-text'
      />
    </React.Fragment>
  );
}
