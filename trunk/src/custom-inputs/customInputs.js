import { DateField } from './DateField';

export function CardDefaultParameterInputs() {
  return <div />;
}

export const customFormInputs = {
  date: {
    displayName: 'Date',
    matchIf: [
      {
        types: ['string'],
        format: 'date',
      },
    ],
    defaultDataSchema: {
      format: 'date',
    },
    defaultUiSchema: {},
    type: 'string',
    cardBody: DateField,
    modalBody: CardDefaultParameterInputs,
  }
}
